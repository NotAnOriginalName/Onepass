// ----
// Init variables
// ----

let DEBUG = 1;
// if set, clear local storage. help for debug.
let CLEAR_STORAGE = 1;
let HOST = "localhost:3000";

/*let encrypted = CryptoJS.AES.encrypt("Message", "Secret Passphrase");
let decrypted = CryptoJS.AES.decrypt(encrypted, "Secret Passphrase");
console.log(decrypted.toString(CryptoJS.enc.Utf8));*/

if(CLEAR_STORAGE){
	browser.storage.local.clear();
}

let p0 = document.getElementById("page0");
let p1 = document.getElementById("page1");
let p2 = document.getElementById("page2");
let loading = document.getElementById("loading");
let cons = document.getElementById("cons");

//cons.innerHTML = "omfg";

let hashOnePass;

// ----
// Register all events listener
// ----

// Encryption key creation
p0.querySelector("input:last-child").addEventListener("click", (e) => {
    let username = p0.querySelector("input[name='username']").value;
    let pass1 = p0.querySelector("input[name='password1']").value;
    let pass2 = p0.querySelector("input[name='password2']").value;
    if(pass1 !== pass2){
        p0.querySelector(".error").innerHTML = "Passwords are differents ! Try again."
        return;
    }
    let buffer = new TextEncoder("utf-8").encode(pass1);
    let hashPromise = crypto.subtle.digest("SHA-256", buffer);
    hashPromise.then( (hash) => {
        return makeRequest({
            method: "POST",
            url: "http://"+HOST+"/register/"+username+"/"+hex(hash)
        });
    })
    .then( (response) => {
        hashOnePass = hex(hash);
        browser.storage.local.set({"hashOnePass":hashOnePass});
        p0.style.display = "none";
        p1.style.display = "block";
    })
    .catch( (e) => {
        if(e.status === undefined)
            p0.querySelector(".error").innerHTML = "Unsupported browser version (couldn't use SHA-256 algorithm)";
        else
            p0.querySelector(".error").innerHTML = e.statusText;
    });
});

// Click on "Add password" in the base menu to add a new password for a site
p1.querySelector("input:last-child").addEventListener("click", (e) => {
    p1.style.display = "none";
    p2.style.display = "block";
});

// Cancel the add of a new password
p2.querySelector("input[type='button']:first-child").addEventListener("click", (e) => {
    p1.style.display = "block";
    p2.style.display = "none";
});

// Add & save a new password for a site
p2.querySelector("input[type='button']:last-child").addEventListener("click", (e) => {
    let OnePass = p2.querySelector("input[name='OnePass']");
    let pass1 = p2.querySelector("input[name='password1']");
    let pass2 = p2.querySelector("input[name='password2']");
    let site = p2.querySelector("input[name='site']");
    if(pass1.value !== pass2.value){
        p2.querySelector(".error").innerHTML = "Passwords are different ! Try again.";
        return;
    }

    // Encrypt password and send it to server
    checkOnePass(pass1.value, hashOnePass)
    .then( (e) => {
        let password = CryptoJS.AES.encrypt(message, pass1.value);
        return makeRequest({
            method: "POST",
            url: "http://"+HOST+"/password/"+username+"/"+hashOnePass+"/"+site.value+"/"+password
        });
    })
    .then( (e) => {
        p1.style.display = "block";
        p2.style.display = "none";
    })
    .catch( (e) => {
        if(e.error)
            p2.querySelector(".error").innerHTML = e.error;
        else
            p2.querySelector(".error").innerHTML = e.statusText;
    });
});

// ----
// Core application
// ----

// When application start, check if it is started for the first time or has been reset
// If it is the first time, start tutorial to create new encryption key
let hashOnePassPromise = browser.storage.local.get("hashOnePass");
hashOnePassPromise.then((e) => {
    if(e.hashOnePass === undefined){
        p0.style.display = "block";
    }else{
        hashOnePass = e.hashOnePass;
        p0.style.display = "none";
        p1.style.display = "block";  
    }
    loading.style.display = "none"; 
}, (err) => {

});

// get current tab
let t = browser.tabs.query({active: true, currentWindow: true});

// get current tab url and set it in the input
t.then((tabs) => {
    let url = new URL(tabs[0].url).hostname;
    document.getElementsByName("site")[0].value = url;
});